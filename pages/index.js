import Layout from "../layouts/Layout";
import UserList from "../components/users-list";
import { host } from "../configs";
import { Row, Col, Form } from "react-bootstrap";
import React from "react";
import Pagination from "../components/pagination";

export default function Home({ usersApi }) {
  const [users, setUsers] = React.useState(usersApi.data);

  // Menggunakan serverSideRendering
  // const [users, setUsers] = React.useState([]);

  // React.userEffect (() => {
  // (async function() {
  //   const response = await host.get('users?page=2');
  //   const { data } = response.data;
  //   setUsers(data);
  // })();
  // }), []);
  const funcFilter = (e) => {
    const value = e.target.value;

    const filterData = users.filter((user) => {
      return (
        user.first_name.search(value) != -1 ||
        user.last_name.search(value) != -1
      );
    });

    if (value.length == 0 || value.trim() == "") {
      (async function () {
        const res = await host.get("users?page=2");
        const { data } = res.data;
        setUsers(data);
      })();
    }

    setUsers(filterData);
  };


  return (
    <div>
      <>
        <Layout>
          <Row className="justify-content-md-end mb-3">
            <Col md={3}>
              <Form.Control
                type="text"
                placeholder="Search users ..."
                onChange={funcFilter}
              />
            </Col>
          </Row>

          <Pagination
            data={users}
            RenderComponent={UserList}
            title="Posts"
            pageLimit={users.length / 3}
            dataLimit={3}
          />
        </Layout>
      </>
    </div>
  );
}

export async function getServerSideProps(context) {
  const res = await host.get("users?page=2");
  const data = res.data;

  return {
    props: { usersApi: data }, // will be passed to the page component as props
  };
}