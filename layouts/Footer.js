import React from "react";
import {footer, div} from 'react-bootstrap';

export default function Footer() {
    return (
        <footer>
            <div className="sticky-footer">
                <div className="container my-auto">
                    <div className="copyright text-center mr-auto pb-3">
                        <span>Copyright &copy; My Website </span>
                    </div>
                </div>
            </div>
        </footer>
    );
}