import React from "react";
import { Row } from "react-bootstrap";
import UserItem from "./users-item";

function UserList({ data }) {
  return (
    <Row className="gy-2">
      <UserItem user={data} />
    </Row>
  );
}

export default UserList;
